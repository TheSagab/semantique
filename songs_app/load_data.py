from rdflib import ConjunctiveGraph, Namespace, Literal
from rdflib.store import NO_STORE, VALID_STORE

from tempfile import mktemp

if __name__ == '__main__':
    path = mktemp()

    # Open previously created store, or create it if it doesn't exist yet
    graph = ConjunctiveGraph('Sleepycat')

    rt = graph.open(path, create=False)

    if rt == NO_STORE:
        # There is no underlying Sleepycat infrastructure, create it
        graph.open(path, create=True)
    else:
        assert rt == VALID_STORE, 'The underlying store is corrupt'

    print('Triples in graph before add: ', len(graph))

    # Now we'll add some triples to the graph & commit the changes
    graph.parse('./result4.ttl', format='n3')

    print('Triples in graph after add: ', len(graph))

    # display the graph in RDF/XML
    # print(graph.serialize(format='n3'))

    # close when done, otherwise sleepycat will leak lock entries.
    graph.close()

    graph = None

    # reopen the graph
    graph = ConjunctiveGraph('Sleepycat')
    graph.open(path, create=False)

    print('Triples still in graph: ', len(graph))

    graph.close()
