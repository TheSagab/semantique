from SPARQLWrapper import SPARQLWrapper

import rdflib


def to_uri(word, rdf_type):
    split = word.split(" ")
    for i in range(len(split)):
        split[i] = split[i].capitalize()
    res =  "_".join(split)
    return rdf_type + ':' + res


def local_query(song_name, query_billboard=True):
    print("Querying local data")
    g = rdflib.Graph()
    result = g.parse("result_new.ttl", format='n3')

    print("graph has %s statements." % len(g))

    q = """PREFIX ex: <http://example.org/>
            PREFIX dbo: <http://dbpedia.org/ontology/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            SELECT ?song_title ?artist ?rank ?year ?lyrics
            WHERE {
                ?song dbo:Song ?song_title
                FILTER (regex(?song_title, "%s", "i")) .
                ?song dbo:Artist ?artist .
                ?song foaf:rank ?rank .
                ?song dbo:Year ?year .
                ?song foaf:lyrics ?lyrics .
            }
            ORDER BY ?song_title ?artist ?year"""% song_name

    if not query_billboard:
        q = """PREFIX ex: <http://example.org/>
            PREFIX dbo: <http://dbpedia.org/ontology/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            SELECT DISTINCT ?song_title ?artist
            WHERE {
                ?song dbo:Song ?song_title
                FILTER (regex(?song_title, "%s", "i")) .
                ?song dbo:Artist ?artist .
            }
            ORDER BY ?song_title ?artist"""% song_name

    qres = g.query(q)

    return qres


def dbpedia_query(song_name):
    song_queried = to_uri(song_name, 'dbr')
    print("Querying remote data on dbpedia.org")
    # song_dbr = "dbr:She_Will_Be_Loved"
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setQuery("""
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX dbp: <http://dbpedia.org/property/>
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT ?label ?abstract ?album ?cover
        WHERE {{ 
            {0} rdf:type dbo:MusicalWork  .
            {0} rdfs:label ?label .
            {0} dbo:abstract ?abstract .
            {0} dbo:album ?x .
            {0} dbp:cover ?cover .
            ?x rdfs:label ?album .
            filter(lang(?label) = 'en') .
            filter(lang(?abstract) = 'en') .
            filter(lang(?album) = 'en') .
        }}
    """.format(song_queried))
    sparql.setReturnFormat('json')
    results = sparql.query().convert()
    # pprint.pprint(results["results"]["bindings"])
    try:
        label = results["results"]["bindings"][0]["label"]["value"]
        abstract = results["results"]["bindings"][0]["abstract"]["value"]
        album = results["results"]["bindings"][0]["album"]["value"]
        album_cover = results["results"]["bindings"][0]["cover"]["value"]
        album_cover = album_cover.replace(" ", "_")
        album_cover = "https://en.wikipedia.org/wiki/File:" + album_cover
        print("Album Cover Link: " + album_cover)
        return {'label': label, 'abstract': abstract, 'album': album, 'album_cover': album_cover}
    except:
        return {}
