from django.shortcuts import render
from .rdf_utils import local_query, dbpedia_query

import rdflib
import re

response = {}


# Create your views here.
def index(request):
    html = 'home.html'
    return render(request, html)


def song_detail(request, song_name_url, list_number):

    #problem nya disini geng, ini gue sementara masih pake yg cuman artist sama song name

    song_name = song_name_url.replace("_", " ")
    song_result_local = local_query(song_name, query_billboard=True)


    print(song_name)
    print(list_number)
    row_count = 0

    intended_song = ""
    intended_artist = ""

    brank_array = []
    byear_array = []

    for row in song_result_local:

        if row_count == int(list_number):
            print("in")

            intended_song = row[0]
            intended_artist = row[1]

            response['title'] = row[0]
            response['artist'] = row[1]

            brank_array.append(row[2])
            byear_array.append(row[3])
            response['lyrics'] = row[4]

            song_result_remote = dbpedia_query(row[0])
            try:
                response['album'] = song_result_remote['album']
                response['abstract'] = song_result_remote['abstract']
                response['cover'] = song_result_remote['album_cover']
            except:
                response['album'] = "Album"
                response['abstract'] = "Abstract"
                response['cover'] = "https://d3i1chc4akc81x.cloudfront.net/images/default-img.png"

        elif row_count > int(list_number) and row[0] == intended_song and row[1] == intended_artist:
            brank_array.append(row[2])
            byear_array.append(row[3])

        elif row_count > int(list_number) and row_count != int(list_number):
            break

        row_count += 1

    response['brank'] = brank_array  # get from query
    response['byear'] = byear_array  # get from query

    html = 'song-detail.html'
    return render(request, html, response)


def result(request):
    song_name = ""
    if request.method == 'POST':
        song_name = request.POST["songTitle"]

    song_result = local_query(song_name, query_billboard=False)
    song_result_final = []

    print(song_result)

    for row in song_result:
        print('start')
        print("Song Title: " + row[0])
        print("Artist: " + row[1])

    for songs in song_result:
        songlst = songs[0].split(" ")
        songsplit = song_name.split(" ")
        if(all(x.capitalize() in songlst for x in songsplit)):
            content = [songs[0], songs[1]]
            song_result_final.append(content)

    html = 'result.html'
    response['song_name_url'] = song_name.replace(" ", "_")
    response['song_name'] = song_name
    response['result'] = result
    response['song_detail'] = song_result_final
    return render(request, html, response)
